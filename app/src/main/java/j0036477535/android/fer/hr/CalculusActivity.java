package j0036477535.android.fer.hr;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Activity for calculating the result of the specified arithmetical operation.
 * Addition, subtraction, multiplication and division are supported.
 */
public class CalculusActivity extends AppCompatActivity {

    /**
     * String constant for mapping of the result in the intent bundle.
     */
    public static final String EXTRAS_RESULT = "result";
    /**
     * Result of the executed arithmetical operation.
     */
    private double result;
    /**
     * String containing error message if any occurs.
     */
    private String errorMessage = "";
    /**
     * Result message for the user.
     */
    private String resultMessage;
    /**
     * Executed operation.
     */
    private String operation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculus);

        Bundle mapa = getIntent().getExtras();
        double var1 = mapa.getDouble(FormActivity.EXTRAS_VARA);
        double var2 = mapa.getDouble(FormActivity.EXTRAS_VARB);
        String operator = mapa.getString(FormActivity.EXTRAS_OPER);

        try{
            if (operator.equals("+")){
                result = var1 + var2;
                operation = var1 + " + " + var2;;
            }else if (operator.equals("-")){
                result = var1 - var2;
                operation = var1 + " - " + var2;
            }else if (operator.equals("x")){
                result = var1 * var2;
                operation = var1 + " x " + var2;;
            }else if (operator.equals("/")){
                if (var2 == 0){
                    throw new RuntimeException("dijeljenje s nulom!");
                }
                result = (double) var1 / var2;
                operation = var1 + " / " + var2;
            }
        }catch (Exception e){
            errorMessage = "Izvođenje je bilo neuspješno, uzrok: " + e.getLocalizedMessage();
        }

        if (errorMessage.isEmpty()){
            resultMessage = String.format("Rezultat operacije %s je %.3f.", operation, result);
        }else{
            resultMessage = errorMessage;
        }

        Intent i = new Intent();
        i.putExtra(EXTRAS_RESULT, resultMessage);
        setResult(RESULT_OK,i);
        finish();
    }


}
