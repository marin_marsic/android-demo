package j0036477535.android.fer.hr;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

/**
 * Activity for serving the user, forwarding hs request to CalculusActivity, an showing the result.
 */
public class FormActivity extends AppCompatActivity {

    /**
     * String constant for mapping of the first variable in the intent bundle.
     */
    public static final String EXTRAS_VARA = "varA";
    /**
     * String constant for mapping of the second variable in the intent bundle.
     */
    public static final String EXTRAS_VARB = "varB";
    /**
     * String constant for mapping of the operator in the intent bundle.
     */
    public static final String EXTRAS_OPER = "operator";

    /**
     * TextView for showing the result of the user's request.
     */
    private TextView tvResult;
    /**
     * Input for the first parameter of the arithmetical operation.
     */
    private EditText etFirstVariable;
    /**
     * Input for the second parameter of the arithmetical operation.
     */
    private EditText etSecondVariable;
    /**
     * Spinner for user to choose an operation to execute.
     */
    private Spinner operatorMenu;
    /**
     * String containing the result of the operation to show and to send via e-mail.
     * At the beginning it states that no operations have been executed.
     */
    private String result = "Rezultat operacije nije poznat jer nije obavljena niti jedna operacija.";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        Button btnCalculate = (Button) findViewById(R.id.btnCalculate);

        tvResult = (TextView) findViewById(R.id.tvResult);
        etFirstVariable = (EditText) findViewById(R.id.etFirstNumber);
        etSecondVariable = (EditText) findViewById(R.id.etSecondNumber);
        operatorMenu = (Spinner) findViewById(R.id.spOperators);

        btnCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String variableA = etFirstVariable.getText().toString();
                String variableB = etSecondVariable.getText().toString();
                String operator = operatorMenu.getSelectedItem().toString();

                double varA = 0;
                try {
                    varA = Double.parseDouble(variableA);
                }catch(Exception e){

                }

                double varB = 0;
                try {
                    varB = Double.parseDouble(variableB);
                }catch(Exception e){

                }

                Intent i = new Intent(FormActivity.this, CalculusActivity.class);
                i.putExtra(EXTRAS_VARA, varA);
                i.putExtra(EXTRAS_VARB, varB);
                i.putExtra(EXTRAS_OPER,operator);

                startActivityForResult(i, 666);
            }
        });

        Button btnSendEMail = (Button) findViewById(R.id.btnSendEMail);
        btnSendEMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"ana.baotic@infinum.hr"});
                i.putExtra(Intent.EXTRA_SUBJECT, "0036477535: dz report");
                i.putExtra(Intent.EXTRA_TEXT   , result);
                try {
                    startActivity(Intent.createChooser(i, "Slanje poruke..."));
                } catch (android.content.ActivityNotFoundException ex) {
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode,
                                    int resultCode, Intent data) {

        if (requestCode == 666 && resultCode == RESULT_OK
                && data != null) {

            String result = data.getExtras()
                    .getString(CalculusActivity.EXTRAS_RESULT);

            tvResult.setText(result);
            this.result = result;
        }
    }
}
